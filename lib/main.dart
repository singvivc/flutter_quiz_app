import 'package:flutter/material.dart';
import 'package:flutter_quiz_app/utils/quiz.dart';
import 'package:flutter_quiz_app/utils/result.dart';

void main() => runApp(QuizApplication());

class QuizApplication extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _QuizApplicationState();
  }
}

class _QuizApplicationState extends State<QuizApplication> {
  final _questions = const [
    {
      'questionText': 'What\'s you favourite color?',
      'answers': [
        {'text': 'Blue', 'score': 4},
        {'text': 'Green', 'score': 2},
        {'text': 'Yellow', 'score': 6},
        {'text': 'Black', 'score': 10}
      ],
    },
    {
      'questionText': 'What\'s your favourite animal?',
      'answers': [
        {'text': 'Rabbit', 'score': 1},
        {'text': 'Snake', 'score': 10},
        {'text': 'Dog', 'score': 4},
        {'text': 'Elephant', 'score': 2}
      ],
    },
  ];

  var _questionIndex = 0;
  int _totalScore = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Quiz Application'),
          backgroundColor: Colors.indigo,
        ),
        body: _questionIndex < this._questions.length
            ? Quiz(this._questions, _questionIndex, this._answerQuestion)
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }

  void _answerQuestion(int currentScore) {
    this._totalScore += currentScore;
    setState(() {
      _questionIndex = _questionIndex + 1;
    });
  }

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }
}
