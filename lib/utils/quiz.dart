import 'package:flutter/material.dart';
import 'package:flutter_quiz_app/answer.dart';
import 'package:flutter_quiz_app/question.dart';

class Quiz extends StatelessWidget {
  final int _questionIndex;
  final Function _selectHandler;
  final List<Map<String, Object>> _questions;

  Quiz(this._questions, this._questionIndex, this._selectHandler);

  @override
  Widget build(BuildContext context) {
    var question =
        Question(this._questions[this._questionIndex]['questionText']);
    var answers = (_questions[this._questionIndex]['answers']
            as List<Map<String, Object>>)
        .map((answer) =>
            Answer(() => _selectHandler(answer['score']), answer['text']))
        .toList();
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[question, ...answers],
    );
  }
}
