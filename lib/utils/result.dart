import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int _totalScore;
  final Function resetQuizHandler;

  Result(this._totalScore, this.resetQuizHandler);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            message,
            style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          FlatButton(
            textColor: Colors.blue,
            child: Text('Restart the quiz!',),
            onPressed: this.resetQuizHandler,
          )
        ],
      ),
    );
  }

  String get message {
    String resultText;
    if (_totalScore <= 8) {
      resultText = 'You are awesome and innocent';
    } else if (_totalScore <= 12) {
      resultText = 'Pretty likeable';
    } else if (_totalScore <= 16) {
      resultText = 'You are strange';
    } else {
      resultText = 'You are bad';
    }
    return resultText;
  }
}
